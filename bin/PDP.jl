
module PDP

using DataFrames, ReadStat

export UtilityPatents, num_patents, MergePatents, Patents

# Import Date Functions
year    = Dates.year
quarter = Dates.quarterofyear

# Set the data directory
wd= "data/patents/"
    
"""
```
df = UtilityPatents()
```
Returns a dataframe containing the cleaned utility patents data
"""
function UtilityPatents()
    
    # Load the dataframe
    df = read_dta(wd * "pat76_06_assg.dta")
    
    # Construct the dates
    df[:date] = Date.(df[:gyear], df[:gmonth], df[:gday])
    
    # Drop 

    return df
end

function num_patents(df)
    """
    ```
    num_patents(df)
    ```
    Computes the number of patents by PDPASS, and QuarterYear
    """
    
    # Drop Observations without any id
    df = df[!isna(df[:pdpass]),:]
   
    # Make a Quarterly identifier
    df[:gquarter] = quarter.(df[:date])
    
    # Compute the number of patents by assignee and quarter
    npatents = by(df, [:pdpass, :gyear, :gquarter]) do x
        DataFrame(num_patents=size(x, 1))
    end

    # Construct End of quarter date
    gyear  = npatents[:gyear]
    gmonth = 3*(npatents[:gquarter]-1)+1
    gdate  = Dates.lastdayofquarter(Date.(gyear, gmonth))
    npatents[:date] = gdate

    return npatents[[:pdpass, :date, :num_patents]]
end

function construct_patent_stocks(df)
    
    sort!(df, cols=[order(:pdpass), order(:date)])
    inventory = by(df, [:pdpass]) do x
        DataFrame(date=x[:date], num_patents=cumsum(x[:num_patents]))
    end
    return inventory
end

function dynass()
    """
    Load the Dynamic Assignee Data
    """
    
    df = readtable(wd * "dynass.csv")
end

function pdpcohdr()
    """
    Load the company header data
    """
    df = readtable(wd * "pdpcohdr.csv")

end

function add_gvkey(df)
    """
    Merge in gvkey data to dataframe using the pdpass identifier.  Uses
    mapping from 
    """
    
    # Load Dynamic Assignment Data
    df2 = dynass()

    # Inner Join the Dynamic Assigment Data to our data frame (Only keep
    # pdpass values for which there is a match)
    merged = join(df, df2, on=:pdpass, kind=:inner)
    merged[:gvkey] = 0
    dtyear = year(merged[:date])
    for i=1:5
        start = merged[Symbol(:begyr, i)]
        stop  = merged[Symbol(:endyr, i)]
        gvkey = merged[Symbol(:gvkey, i)]
        
        # Pick only the the correct years
        idx = similar(gvkey, Bool)
        for i in eachindex(gvkey)
            if isna(gvkey[i])
                idx[i] = false
            else
                if start[i] .<= dtyear[i] .<= stop[i]
                    idx[i] = true
                else
                    idx[i] = false
                end
            end
        end
        merged[idx, :gvkey] = gvkey[idx]
    end
    
    # Drop the guys we haven't matched
    nomatch = merged[:gvkey] .== 0
    merged = merged[!nomatch, :]

    # Drop the irrelevant Data
    cols = names(df)
    push!(cols, :gvkey)

    # Sum over the remaining columns
    combined = by( merged[cols], [:gvkey, :date]) do x
        out = DataFrame()
        for name in names(x)
            if !(name in [:gvkey, :date])
                out[name] = sum(x[name])
            end
        end
        return out
    end

    return combined
end



# Put it all together
function Patents()
    
    df = UtilityPatents()
    
    # Compute the cumulative patents stocks by date and pdpass
    npatents = num_patents(df)
    
    # Merge in gvkey using dynamic matching
    matched = add_gvkey(npatents)
    
    return matched
end

function MergePatents(df)
    
    # Compute the patent stocks if they haven't already been provided
    patents = Patents()
    
    # Return the merged data
    return MergePatents(df, patents)
end

"""
```
MergePatents(df [, patents])
```
Merges in cumulative patent totals to the given dataframe with gvkey and
year as the identifying incices.  
"""
function MergePatents(df, patents)
    
    # Merge in the patent stocks
    matched = join(df, patents, on=[:date, :gvkey], kind=:left)
    
    # Let's handle the companies that don't have any patents (they might
    # still have positive R&D expenditures)
    
    # Load the Company Header File and only use the records for which we
    # know there is not a match (i.e., co_header[:match] .== 0)
    co_header = pdpcohdr()
    matched = join(matched, co_header, on=[:gvkey], kind=:left)
    
    # Throw out the values for which we don't know the match
    matched = matched[!isna(matched[:match]),:]

    # Set all the missing number of patents to zero -- now that we've
    # thrown out the unknown matches, we know that if we don't observe a
    # patent, then they didn't get one
    matched[isna(matched[:num_patents]), :num_patents] = 0
    
    output = matched[ [:gvkey, :date, :cusip, :conm, :curcdq, :xrdq,
        :xrdy, :num_patents]]
    
    return output

end

end
