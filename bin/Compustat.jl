module Compustat

using Query, DataFrames, ReadStat, CSV

export xrd_data

# Import Date Functions
year    = Dates.year
quarter = Dates.quarterofyear

# Set the data directory
wd= "data/compustat/"

function xrd_data()
    """
    ```
    df = xrd_data()
    ```
    Returns a dataframe containing the cleaned research and development
    data
    """
    
    df = readtable(wd * "quarterly_xrd.csv")
    
    # Convert Dates
    fmt = DateFormat("yyyymmdd")
    df[:date] = Date.(string.(df[:datadate]), fmt)

    return df

end

end
