#=
Title:  XRD_Patents.jl
Author: Jacob Adenbaum
Date:   October 15, 2017

Puts together the cleaned compustat data and NBER Patents data in order
to compute the cross sectional relationship between corporate R&D
investment and patents
=#

# External Modules
using DataFrames, FredData, PyPlot

# My Modules for this project
using Compustat, PDP

function PrepData()
    # Pull Inflation Data
    f = Fred()
    pce = (get_data(f, "DPCCRG3A086NBEA").df)[[:date, :value]]
    rename!(pce, :value, :pce)
    pce[:year] = Dates.year(pce[:date])
    pce = pce[[:year, :pce]]


    # Load Compustat Data and only keep the data in the correct time
    # range
    cpst = xrd_data()
    cpst = cpst[Dates.year(cpst[:date]) .<= 2006,:]

    # Load and Merge Patents Data
    pats = Patents()
    df   = MergePatents(cpst, pats)

    # Turns out we actually want annual data since it's higher quality than
    # the quarterly data
    df[:year] = Dates.year(df[:date])
    annual = by(df, [:gvkey, :year]) do row
        xrd = row[:xrdy]
        pats = row[:num_patents]
        
        # Sum the data annually
        DataFrame(xrd=sum(xrd[!isna(xrd)]), num_patents=sum(pats))
    end

    # Merge in PCE Data and get R&D expenditures in real terms
    annual = join(annual, pce, on=:year, kind=:left)
    annual[:xrd] .*= 100./annual[:pce]

    return annual
end

function make_scatter(df, outpath)
    # Compute the average by year
    byfirm = by(df, :gvkey) do x
        DataFrame(xrd=mean(x[:xrd]), num_patents=mean(x[:num_patents]))
    end

    # Drop the guys at zero on either side
    nonzero = (byfirm[:xrd] .> 0) & (byfirm[:num_patents] .> 0)

    # Without the zero guys
    fig, ax = subplots()
    xrd     = byfirm[nonzero, :xrd]
    npats   = byfirm[nonzero, :num_patents]

    scatter(log.(xrd), log.(npats),
        facecolors="none",
        edgecolors="r",
        alpha=.25)

    xlabel("log(R&D Investment)")
    ylabel("log(patents)")
    title("Average Yearly Patents vs. Annual R&D Investment\n"*
            "By Firm")

    savefig(outpath)
    close(fig)
end

# Run the code
df = PrepData()

make_scatter(df, "output/xrd_patents.pdf")
