A repository for code on replicating the stylized facts from Klette and
Kortum:

# Stylized Facts
Klette and Kortum report several stylized facts.  For this project,
we're going to be replicating five of them, listed below.

## Stylized Fact 2
Patents and R&D are positively related across firms

* Grilches (1990) on the relationship between innovation, patents, and R&D
* Cohen and Klepper (1996) on the high patent-R&D ratio among small
firms

## Stylized Fact 3
R&D Intensity is independent of firm size

* Cohen (1995) on R&D expenditures vs firm size
* Cohen and Klepper (1996) on the same

## Stylized Fact 4
The distribution of R&D intensity is highly skewed, and a large fraction
of firms report zero R&D

* Cohen (1995) for within firm analysis
* Cohen and Klepper (1992) on the cross industry analysis
* Klette and Johansen (1998) on observing the same pattern in
  norwegian firms

## Stylized Fact 5
Differences in R&D intensity across firms are highly persistent.

* Scott (1984) shows that in a large longitudinal sample of firms, 50%
  of the variance in business unit R&D intensity is accounted for by
  firm fixed effects

## Stylized Fact 6
Firm R&D investment follows essentially a geometric random walk

* Hall et al. (1986)
* Klette and Griliches (2000) find zero correlation between changes in
  log R&D and the level of R&D for Norwegian firms

# Data
In order to verify these ``facts'' we're going to need firm level data
on the following variables:

1. R&D Investment
    
    * Compustat data series XRD

2. Number of patents

  * We're getting this data from the NBER Patent Data Project
3. Firm size (in number of employees)
4. Firm size (revenues)
5. R&D intensity: This is just R&D expenditures as a fraction of firm
   revenues

